  # -*- coding: utf-8 -*-
from frutasyverduras_v2 import items
from frutasyverduras_v2 import settings
from frutasyverduras_v2.items import *
import csv


class FrutasyverdurasV2Pipeline(object):
	def __init__(self):
		self.file_name = {}  
		self.keys_telemercado = {'producto','id_producto','categoria','detalles_precios','url_imagen','url','fuente','fecha_extraccion','hora_extraccion'}
		self.keys_jumbo = {'producto','id_producto','precio_medida','precio','detalles_precios','marca','url_imagen','url','fuente','fecha_extraccion','hora_extraccion'}
	   
	def open_spider(self, spider):
		self.file_name=csv.writer(open('output_'+spider.name+'.csv','w'))
		if spider.name.find('telemercado')!=-1:
			self.file_name.writerow(['producto','id_producto','categoria','detalles_precios','url_imagen','url','fuente','fecha_extraccion','hora_extraccion'])
		if spider.name.find('jumbo_cl')!=-1:
			self.file_name.writerow(['producto','id_producto','precio_medida','precio','detalles_precios','marca','url_imagen','url','fuente','fecha_extraccion','hora_extraccion'])
	def process_item(self, item, spider):
		if spider.name.find('telemercado')!=-1:  
			for key in self.keys_telemercado:
				item.setdefault(key,'NA')      
			self.file_name.writerow([
									item['producto'],
									item['id_producto'],
									item['categoria'],
									item['detalles_precios'],
									item['url_imagen'],
									item['url'],
									item['fuente'],
									item['fecha_extraccion'],
									item['hora_extraccion']
							])
		
			return item
		if spider.name.find('jumbo_cl')!=-1:  
			for key in self.keys_jumbo:
				item.setdefault(key,'NA')      
			self.file_name.writerow([
									item['producto'],
									item['id_producto'],
									item['precio_medida'],
									item['precio'],
									item['detalles_precios'],
									item['marca'],
									item['url_imagen'],
									item['url'],
									item['fuente'],
									item['fecha_extraccion'],
									item['hora_extraccion'],
							])
		
			return item


