# -*- coding: utf-8 -*-
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from scrapy.http import Request
from frutasyverduras_v2.items import SuperLoader
import time
from w3lib.url import url_query_parameter



class jumbo_cl(CrawlSpider):
    name = 'jumbo_cl'
    #allowed_domains = ['telemercado.cl']
    start_urls = ["http://www.jumbo.cl/FO/CategoryDisplay?cab=4006&int=11&ter=124"]
    urls = [  
     #Frutas
    "http://www.jumbo.cl/FO/PasoDosResultado?cab=4006&int=11&ter=124",
    #Verduras
    "http://www.jumbo.cl/FO/PasoDosResultado?cab=4006&int=11&ter=125",
    #Frutos secos
    "http://www.jumbo.cl/FO/PasoDosResultado?cab=4006&int=11&ter=123"
    ]
    def parse(self,response):
        for link in self.urls:
            yield Request(url=link
                             ,dont_filter=True
                             ,callback=self.parse_data)

    def parse_data(self,response):
        for row in response.xpath("//ul[@class='grid']/li"):
            loader = SuperLoader(response=response, selector = row)
            url = row.xpath(".//img[@id='img_{pro_id}']/@src").extract_first()
            marca = row.xpath(".//span[@class='Marca']//text()").extract_first()
            detalles_precios = row.xpath("concat(.//*[@class='txt_precio_h'],' & ',.//*[@class='txt_precio_medida_h'])").extract_first()
            precio_antes = marca

            if not marca:
                marca = 'SIN MARCA'

            if str(precio_antes).find('ANTES')!=-1:
                marca = 'SIN MARCA'
                detalles_precios = detalles_precios + '&' + precio_antes


            
            loader.add_xpath( 'producto'            , ".//div[@class='txt_nombre_h']/a[@id='ficha']//text()")
            loader.add_xpath( 'id_producto'         , ".//div[@class='txt_nombre_h']/a[@id='ficha']/@href")
            loader.add_xpath( 'precio_medida'       , ".//div[@class='txt_precio_medida_h']//text()")
            loader.add_xpath( 'precio'              , ".//div[@class='txt_precio_h']//text()")        
            loader.add_value( 'detalles_precios'    , detalles_precios ) 

            loader.add_value( 'marca'               ,  marca )
            loader.add_value( 'url_imagen'          , 'https://www.jumbo.cl' + url )
            loader.add_value( 'url'                 ,  response.url)
            loader.add_value( 'fuente'              , 'www.jumbo.cl')
            loader.add_value( 'fecha_extraccion'    , time.strftime("%d/%m/%Y"))
            loader.add_value( 'hora_extraccion'     , time.strftime("%H:%M:%S"))

            yield (loader.load_item())


'''if not marca:
                marca = 'SIN MARCA'

            if str(precio_antes).find('ANTES')!=-1:
                marca = 'SIN MARCA'
                if str(precio1) and not str(precio2):
                    detalles_precios = precio1 + '&' + precio_antes
                elif str(precio2) and not str(precio1):
                    detalles_precios = precio2 + '&' + precio_antes
                elif str(precio2) and  str(precio1):
                    detalles_precios = precio1 + '&' + precio2 + '&'+precio_antes

            elif not str(precio_antes):
                if str(precio1) and not str(precio2):
                    detalles_precios = precio1
                elif str(precio2) and  str(precio1):
                    detalles_precios = precio1 + '&' + precio2
                elif str(precio2) and not str(precio1):
                    detalles_precios = precio2
'''