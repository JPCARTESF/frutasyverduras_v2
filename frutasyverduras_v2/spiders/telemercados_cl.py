# -*- coding: utf-8 -*-
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from scrapy.http import Request
from frutasyverduras_v2.items import TelemercadosLoader
import time
from w3lib.url import url_query_parameter



class telemercado_cl(CrawlSpider):
    name = 'telemercado_cl'
    allowed_domains = ['telemercado.cl']
    start_urls = ["https://supermercado.telemercados.cl/openstore/asp/LstProdProductos.asp?argFamiliaId=4"]

    def parse(self,response):
        for link in response.xpath("//*[@class='link-producto']/@href").re(r"argProducto.*"):
            link_clear = str(link).replace("');",'')
            yield Request(url="https://supermercado.telemercados.cl/fichaproducto/ficha_producto.asp?"+link_clear
                         ,dont_filter=True
                         ,callback=self.parse_data)
    def parse_data(self,response):
        loader = TelemercadosLoader(response=response)
        loader.add_xpath( 'producto', "//td[@class='fp_pp_nombre_producto']/text()")
        loader.add_value( 'id_producto', url_query_parameter(response.url, 'argProductoId'))
        loader.add_value( 'categoria', url_query_parameter(response.url, 'argFamilia') +'-'+url_query_parameter(response.url, 'argSubFamilia'))
        loader.add_xpath( 'detalles_precios', "//td[@class='fp_pp_det_tabla_carac']/b//text()")
        url = response.xpath("//td[@class='fp_pp_foto_grande']/img[1]/@src").extract_first()
        loader.add_value( 'url_imagen','https://supermercado.telemercados.cl' + url.replace('..',''))
        loader.add_value( 'url', response.url)
        loader.add_value( 'fuente','www.telemercado.cl')
        loader.add_value( 'fecha_extraccion', time.strftime("%d/%m/%Y"))
        loader.add_value( 'hora_extraccion', time.strftime("%H:%M:%S"))

        yield (loader.load_item())
        
