PATH=$PATH:/usr/local/bin
export PATH

cd /home/spot1/frutasyverduras_v2/frutasyverduras_v2/spiders/

scrapy runspider telemercados_cl.py

echo 'Archivos csv generados por script del mismo nombre sin la palabra output_ y extension .csv' >> errores.csv

for entry in $(ls output_*)
do
   if [ -s ${entry} ];then
       #Se copia tanto en frutas y verduras nuevos como frutas y verduras old
       aws s3 cp ${entry} s3://spot-frutasyverduras-old/descargasScrapy/outputfiles/archivos_sep/`date +%Y%m%d_%H`_d/
       aws s3 mv ${entry} s3://spot-backup/spot-frutasyverduras-old/descargasScrapy/outputfiles/archivos_sep/`date +%Y%m%d_%H`_d/

       aws s3 cp ${entry} s3://spot-frutasyverduras/descargasScrapy/outputfiles/archivos_sep/`date +%Y%m%d_%H`_d/
       aws s3 mv ${entry} s3://spot-backup/spot-frutasyverduras/descargasScrapy/outputfiles/archivos_sep/`date +%Y%m%d_%H`_d/
   else
       echo  ${entry} >> errores.csv
   fi
done


aws s3 cp  errores.csv s3://spot-backup/spot-frutasyverduras-old/descargasScrapy/outputfiles/errores/`date +%Y%m%d_%H`_d.csv
aws s3 cp  errores.csv s3://spot-backup/spot-frutasyverduras/descargasScrapy/outputfiles/errores/`date +%Y%m%d_%H`_d.csv

aws s3 cp  errores.csv s3://spot-frutasyverduras-old/descargasScrapy/outputfiles/errores/`date +%Y%m%d_%H`_d.csv
aws s3 mv  errores.csv s3://spot-frutasyverduras/descargasScrapy/outputfiles/errores/`date +%Y%m%d_%H`_d.csv


find . -name \*.csv -delete

cd /home/spot1/cronlog
aws s3 mv bash_`date +%Y%m%d_%H`_d.log  s3://spot-frutasyverduras/descargasScrapy/bash_run/bash_`date +%Y%m%d_%H`_d.log
find . -name \*.log -delete


cd /home/spot1/frutasyverduras_v2
find . -name \*.pyc -delete
