# -*- coding: utf-8 -*-
import scrapy
from scrapy.item import Item
from scrapy.loader import ItemLoader, XPathItemLoader
from scrapy.loader.processors import TakeFirst, MapCompose, Join, Compose
import re
import time

class FrutasyverdurasV2Item(scrapy.Item):
    #TELEMERCADO
    detalles_precios  = scrapy.Field()
    detalles_tecnicos = scrapy.Field()
    nombre= scrapy.Field()
    envase= scrapy.Field()
    id_producto=  scrapy.Field()
    categoria=  scrapy.Field()
    detalles_precios=  scrapy.Field()
    producto = scrapy.Field()

    #Jumbo
    marca = scrapy.Field()
    precio_medida = scrapy.Field()
    precio = scrapy.Field()

    #Valores estáticos
    url_imagen=  scrapy.Field()
    hora_extraccion=  scrapy.Field()
    fecha_extraccion=  scrapy.Field()
    fuente=  scrapy.Field()
    url =  scrapy.Field()

class SuperLoader(ItemLoader):
    default_item_class = FrutasyverdurasV2Item

    def strip_dashes(value):
        if value:
            return value.replace('\t','').replace('\n','').replace('\r','').replace(' ','').replace('*','').replace('\xa0','').replace('.','').strip()
    def strip_prices(value):
        if value:
            return value.replace('\t','').replace('\n','').replace('\r','').replace("$",'').replace(".",'').replace('& &','&').replace(' ','')

    def strip_id_producto(value):
        if value:
            return value.replace('\t','').replace('\n','').replace('\r','').replace("javascript:",'').replace(".",'').replace(',','').replace(';','').replace(' ','').replace(')','').replace('(','')
    
    categoria_in  = Compose(Join(),strip_dashes)
    #Datos no estructurados
    detalles_precios_in = Compose(Join(),strip_prices)
    precio_medida_in = Compose(Join(),strip_prices)
    precio_in = Compose(Join(),strip_prices)
    id_producto_in = Compose(Join(),strip_id_producto)


    producto_out =TakeFirst()
    envase_out =TakeFirst()
    marca_out = TakeFirst()
    precio_out = TakeFirst()
    #Valores de salida
    precio_medida_out = TakeFirst()
    id_producto_out = TakeFirst()
    url_imagen_out = TakeFirst()
    hora_extraccion_out = TakeFirst()
    fecha_extraccion_out = TakeFirst()
    fuente_out = TakeFirst()
    url_out  = TakeFirst()
    categoria_out = TakeFirst()
    detalles_precios_out = TakeFirst()
    id_producto_out = TakeFirst()



class TelemercadosLoader(ItemLoader):
    default_item_class = FrutasyverdurasV2Item

    def price_processor(v):
        if v:
            return v.replace('.', '').replace('$','').replace('*','')
    def strip_dashes(value):
        if value:
            return value.replace('\t','').replace('\n','').replace('\r','').replace(' ','').replace('*','').replace('\xa0','').replace('.','').strip()
    def strip_prices(value):
        if value:
            return value.replace('\t','').replace('\n','').replace('\r','')


    categoria_in  = MapCompose(Join(),strip_dashes)
    #Datos no estructurados
    detalles_precios_in = MapCompose(Join(),strip_dashes)
    detalles_tecnicos_in = MapCompose(Join(),strip_dashes)
    producto_out =TakeFirst()
    envase_out =TakeFirst()



    #Valores de salida
    id_producto_out = TakeFirst()
    url_imagen_out = TakeFirst()
    hora_extraccion_out = TakeFirst()
    fecha_extraccion_out = TakeFirst()
    fuente_out = TakeFirst()
    url_out  = TakeFirst()
    categoria_out = TakeFirst()
    detalles_precios_out = TakeFirst()
    #producto_out = TakeFirst()



